/**
 * Created by Francis Masse on 2019-07-12.
 */

var fs = require('fs');
var admin = require("firebase-admin");
var serviceAccount = require("../uptune-6e2c4-firebase-adminsdk-ucdqd-25195266d1.json");
admin.initializeApp({
credential: admin.credential.cert(serviceAccount),
databaseURL: "https://uptune-6e2c4.firebaseio.com"
});

const db = admin.firestore().collection('users');

module.exports = {
    getEmailBetween: async function(from, to) {
        console.log(from, to);
        let emailList = [];
        emailList.push(["Email Address"]);
        await db.get()
        .then(snapshot => {
            if (snapshot.empty) {
                console.log('No matching documents.');
                return;
            }
            snapshot.forEach(doc => {
                if (doc.createTime.toDate() >= from && doc.createTime.toDate() < to) {
                    if(doc.data().email !== null) {
                        var email = doc.data().email;
                        emailList.push([email]);
                    }
                }
            })
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });

        const CSVString = emailList.join('\n');
        const nameDate = from.toLocaleDateString().split("/").join("_") + '_' + to.toLocaleDateString().split("/").join("_");

        fs.writeFile(`email_${nameDate}.csv`, CSVString, 'utf8', function(err) {
            if (err) {
              console.log('Some error occured - file either not saved or corrupted file saved.', err);
            } else {
              console.log('It\'s saved!');
            }
          });
    }
}