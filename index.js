/**
 * Created by Francis Masse on 2019-07-12.
 */

// const email = require("./email/getEmail");

// var from = new Date(2019, 8, 24, 0, 0, 0, 0);
// var to = new Date(2019, 9, 3, 0, 0, 0, 0);
// email.getEmailBetween(from, to);

var admin = require("firebase-admin");
// var serviceAccount = require("./uptune-6e2c4-firebase-adminsdk-ucdqd-25195266d1.json");
var serviceAccount = require("./uptune-dev-firebase-adminsdk-8sqg9-116cbdaae3.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://uptune-6e2c4.firebaseio.com"
});
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(
  "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"
);

const users = admin.firestore().collection("users");

// const email = 'piperseneca2@gmail.com';
// const name = 'Jean';
// const family_name = '';

// admin
//   .auth()
//   .createUser({
//     email: email,
//     emailVerified: true,
//     password: 'Upscale123',
//     displayName: 'Jean ',
//     disabled: false
//   })
//   .then(async function(userRecord) {
//     // See the UserRecord reference doc for the contents of userRecord.
//     console.log('Successfully created new user:', userRecord.uid);
//     const currentDate = new Date();
//     const timezone = -4;

//     const dbUser = admin
//       .firestore()
//       .collection('users')
//       .doc(userRecord.uid);
//     await dbUser.get().then(async doc => {
//       if (!doc.exists) {
//         await dbUser.set({
//           lastUsage: currentDate,
//           timezone,
//           id: userRecord.uid,
//           email,
//           name,
//           family_name,
//           bg_color: `hsl(${Math.floor(Math.random() * 360)}, 50%, 70%)`,
//           feedbackAlert: false
//         });
//       }
//     });
//   })
//   .catch(function(error) {
//     console.log('Error creating new user:', error);
//   });

var twoMonths = new Date();
twoMonths.setDate(twoMonths.getDate() - 10);

let htmlEmailList = [];

users
  .where("lastUsage", "<=", twoMonths)
  .limit(1000)
  .get()
  .then(snapshot => {
    if (snapshot.empty) {
      console.log("No matching documents.");
      return false;
    }

    snapshot.forEach(doc => {
      const userData = doc.data();
      const { email, name, unsubscribed, noUsageTwoMonths } = userData;

      if (email !== null && !unsubscribed && !noUsageTwoMonths) {
        users.doc(doc.id).update({ noUsageTwoMonths: true });
        const msg = {
          to: email,
          from: "UPTUNE 🎸🎵 <hi@upscale-technology.com>",
          subject: "why?",
          templateId: "d-32460239126745409d6c3a5276b1cde3",
          personalizations: [
            {
              to: [
                {
                  email: email
                }
              ],
              dynamic_template_data: {
                username: name
              }
            }
          ]
        };
        htmlEmailList.push(msg);
      } else if (unsubscribed) {
        console.log("unsubscribed: " + email);
      }
    });

    return sgMail.send(htmlEmailList, (error, result) => {
      if (error) {
        console.log("Sendgrid Error: " + error);
        return false;
      } else {
        console.log(
          "Succesfully Send Email to " + htmlEmailList.length + " users"
        );
        return true;
      }
    });
  })
  .catch(err => {
    console.log("Error getting documents", err);
  });
