"use strict";

var _firebaseFunctions = require("firebase-functions");

var functions = _interopRequireWildcard(_firebaseFunctions);

var _sendMonthlyLimitEmail = require("./email/sendMonthlyLimitEmail");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

//  export const monthlyEmail = functions.pubsub.topic('monthlyEmail').onPublish(async () => {
//      sendMonthlyLimitEmail();
//  })

/**
 * Created by Francis Masse on 2019-07-12.
 */

(0, _sendMonthlyLimitEmail.sendMonthlyLimitEmail)();
// var from = new Date(2019, 6, 4, 0, 0, 0, 0);
// var to = new Date(2019, 6, 12, 0, 0, 0, 0);
// email.getEmailBetween(from, to);