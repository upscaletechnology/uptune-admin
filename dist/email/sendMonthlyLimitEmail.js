"use strict";

/**
 * Created by Francis Masse on 2019-07-12.
 */

var fs = require('fs');
var admin = require("firebase-admin");
var serviceAccount = require("../uptune-6e2c4-firebase-adminsdk-ucdqd-25195266d1.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://uptune-6e2c4.firebaseio.com"
});

var db = admin.firestore().collection('users');

module.exports = {
    sendMonthlyLimitEmail: function sendMonthlyLimitEmail() {
        var nodemailer = require('nodemailer');
        var handlebars = require('handlebars');

        var readHTMLFile = function readHTMLFile(path, callback) {
            fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {
                if (err) {
                    throw err;
                    callback(err);
                } else {
                    callback(null, html);
                }
            });
        };

        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'fmasse@upscale-technology.com',
                pass: 'Joanne13'
            }
        });

        var beginningDateObject = new Date();
        beginningDateObject.setHours(23, 59, 59, 59);
        console.log(beginningDateObject);

        db.where('monthlyLimit.startingDate', '<=', beginningDateObject).limit(100).get().then(function (snapshot) {
            if (snapshot.empty) {
                console.log('No matching documents.');
                return;
            }

            beginningDateObject.setMonth(beginningDateObject.getMonth() + 1);

            snapshot.forEach(function (doc) {
                if (!doc.data().premium) {
                    var newMonthlyLimit = {
                        numberSoundcheck: 0,
                        startingDate: beginningDateObject
                    };

                    db.doc(doc.id).update({ monthlyLimit: newMonthlyLimit });
                    var email = doc.data().email;
                    var name = doc.data().name;

                    readHTMLFile('./email/uptune-monthly-soundcheck.html', function (err, html) {
                        var template = handlebars.compile(html);
                        var replacements = {
                            username: name
                        };
                        var htmlToSend = template(replacements);
                        var mailOptions = {
                            from: '"UPTUNE 🎸🎵" <hi@upscale-technology.com>',
                            to: email,
                            subject: 'We just added 3 Soundchecks to your account',
                            html: htmlToSend
                        };

                        transporter.sendMail(mailOptions, function (error, info) {
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });
                    });
                }
            });
        }).catch(function (err) {
            console.log('Error getting documents', err);
        });
    },
    getEmailBetween: async function getEmailBetween(from, to) {
        console.log(from, to);
        var emailList = [];
        emailList.push(["Email Address"]);
        await db.get().then(function (snapshot) {
            if (snapshot.empty) {
                console.log('No matching documents.');
                return;
            }
            snapshot.forEach(function (doc) {
                if (doc.createTime.toDate() >= from && doc.createTime.toDate() < to) {
                    if (doc.data().email !== null) {
                        var email = doc.data().email;
                        emailList.push([email]);
                    }
                }
            });
        }).catch(function (err) {
            console.log('Error getting documents', err);
        });

        var CSVString = emailList.join('\n');
        var nameDate = from.toLocaleDateString().split("/").join("_") + '_' + to.toLocaleDateString().split("/").join("_");

        fs.writeFile("email_" + nameDate + ".csv", CSVString, 'utf8', function (err) {
            if (err) {
                console.log('Some error occured - file either not saved or corrupted file saved.', err);
            } else {
                console.log('It\'s saved!');
            }
        });
    }
};