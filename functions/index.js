const functions = require("firebase-functions");
var admin = require("firebase-admin");
var serviceAccount = require("./uptune-6e2c4-firebase-adminsdk-ucdqd-25195266d1.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://uptune-6e2c4.firebaseio.com"
});
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(
  "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"
);

const users = admin.firestore().collection("users");

exports.monthlyEmail = functions.pubsub
  .topic("monthlyEmail")
  .onPublish(async () => {
    var beginningDateObject = new Date();
    beginningDateObject.setUTCHours(23, 59, 59, 59);
    console.log(beginningDateObject);

    let htmlEmailList = [];

    return users
      .where("monthlyLimit.startingDate", "<=", beginningDateObject)
      .limit(1000)
      .get()
      .then(snapshot => {
        if (snapshot.empty) {
          console.log("No matching documents.");
          return false;
        }

        var newDate = new Date();
        newDate.setMonth(newDate.getMonth() + 1);

        snapshot.forEach(doc => {
          const userData = doc.data();
          const { email, name, premium, unsubscribed } = userData;
          const newMonthlyLimit = {
            numberSoundcheck: 0,
            startingDate: newDate
          };

          if (email !== null && !premium && !unsubscribed) {
            users.doc(doc.id).update({ monthlyLimit: newMonthlyLimit });
            const msg = {
              to: email,
              from: "UPTUNE 🎸🎵 <hi@upscale-technology.com>",
              subject: "We just added 3 Soundchecks to your account",
              templateId: "d-c28290d9f9a2440c9158b6f477333dfb",
              personalizations: [
                {
                  to: [
                    {
                      email: email
                    }
                  ],
                  dynamic_template_data: {
                    username: name
                  }
                }
              ]
            };
            htmlEmailList.push(msg);
          } else {
            users.doc(doc.id).update({ monthlyLimit: newMonthlyLimit });
          }
        });

        return sgMail.send(htmlEmailList, (error, result) => {
          if (error) {
            console.log("Error from Sendgrid: " + error);
            return false;
          } else {
            console.log(
              "Succesfully Send Email to " + htmlEmailList.length + " users"
            );
            return true;
          }
        });
      })
      .catch(err => {
        console.log("Error getting documents", err);
      });
  });

exports.twoMonthsInactivity = functions.pubsub
  .topic("twoMonthsInactivity")
  .onPublish(async () => {
    var twoMonths = new Date();
    twoMonths.setDate(twoMonths.getDate() - 60);

    let htmlEmailList = [];

    return users
      .where("lastUsage", "<=", twoMonths)
      .limit(1000)
      .get()
      .then(snapshot => {
        if (snapshot.empty) {
          console.log("No matching documents.");
          return false;
        }

        snapshot.forEach(doc => {
          const userData = doc.data();
          const { email, name, unsubscribed, noUsageTwoMonths } = userData;

          if (email !== null && !unsubscribed && !noUsageTwoMonths) {
            users.doc(doc.id).update({ noUsageTwoMonths: true });
            const msg = {
              to: email,
              from: "UPTUNE 🎸🎵 <hi@upscale-technology.com>",
              subject: "why?",
              templateId: "d-32460239126745409d6c3a5276b1cde3",
              personalizations: [
                {
                  to: [
                    {
                      email: email
                    }
                  ],
                  dynamic_template_data: {
                    username: name
                  }
                }
              ]
            };
            htmlEmailList.push(msg);
          } else if (unsubscribed) {
            console.log("unsubscribed: " + email);
          }
        });

        return sgMail.send(htmlEmailList, (error, result) => {
          if (error) {
            console.log("Sendgrid Error: " + error);
            return false;
          } else {
            console.log(
              "Succesfully Send Email to " + htmlEmailList.length + " users"
            );
            return true;
          }
        });
      })
      .catch(err => {
        console.log("Error getting documents", err);
      });
  });
